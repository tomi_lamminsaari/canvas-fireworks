# README #

This is an example of how to use HTML5 Canvas element to draw animated graphics. This page contains basically
a single canvas-element and all the animation is being drawn by the Javascript code.

You can see the final result here: https://tomilamminsaari.z16.web.core.windows.net/fireworks/index.html

### How do I get set up? ###

No need to setup anything. Just clone the repository and open the page/index.html in your browser to view the page.

![Screenshot](https://bitbucket.org/tomi_lamminsaari/canvas-fireworks/raw/c63b3c32965dd85febf7a1f87ab29b23adefc172/screenshot.png)