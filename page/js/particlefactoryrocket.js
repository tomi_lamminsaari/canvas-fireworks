
var ROCKET_COLOR_SCHEME_RGB = 0;
var ROCKET_COLOR_SCHEME_BLUE = 1;
var ROCKET_COLOR_SCHEME_WHITE = 2;

function getRocketParticleColor(colScheme)
{
    var v = Math.random();
    if (colScheme === ROCKET_COLOR_SCHEME_RGB) {
        if (v < 0.3) {
            return newRgbColor(255, 0, 0);
        } else if (v < 0.6) {
            return newRgbColor(255, 255, 0);
        } else {
            return newRgbColor(120, 255, 0);
        }
    } else if (colScheme == ROCKET_COLOR_SCHEME_BLUE) {
        if (v < 0.3) {
            return newRgbColor(120, 180, 255);
        } else if (v < 0.6) {
            return newRgbColor(80, 130, 230);
        } else {
            return newRgbColor(30, 80, 255);
        }
    } else if (colScheme == ROCKET_COLOR_SCHEME_WHITE) {
        if (v < 0.3) {
            return newRgbColor(255,255,255);
        } else if (v < 0.6) {
            return newRgbColor(220, 220, 220);
        } else {
            return newRgbColor(180, 180, 180);
        }
    }
    return newRgbColor(180, 180, 180);
}

function ParticleFactoryRocket(particleCount, colScheme)
{
    this.particleCount = particleCount;
    this.colorScheme = colScheme;
    
    this.createParticles = function(particleSystem) {
        var dirVec = newVec2(85.0, 0.0);
        var particlesCount1 = Math.floor(this.particleCount / 2);
        var angleStep = 360.0 / particlesCount1;
        var a = 0.0;
        for (var i=0; i < particlesCount1; ++i) {
            dirVec.setXY(60.0 + (30.0 * Math.random()), 0.0);
            dirVec.rotateDegs(a + 10.0 * Math.random());
            var p = new Particle();
            p.initialize(newVec2(0.0, 0.0), 3.0 + (0.8 * Math.random()), dirVec);
            p.color = getRocketParticleColor(this.colorScheme);
            particleSystem.addParticle(p);
            
            dirVec.setXY(26.0 + (30.0 * Math.random()), 0.0);
            dirVec.rotateDegs(a + 10.0 * Math.random());
            p = new Particle();
            p.initialize(newVec2(0.0, 0.0), 3.0 + (0.8 * Math.random()), dirVec);
            p.color = getRocketParticleColor(this.colorScheme);
            particleSystem.addParticle(p);
            
            a += angleStep;
        }
    };
}


