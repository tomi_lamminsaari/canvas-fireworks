
function ParticleManager()
{
    this.particleSystems = new Array();
}

/**
 * 
 * @param {Vec2} initialPos
 * @param {Vec2} initialVelocity
 */
ParticleManager.prototype.addSystem = function(initialPos, initialVelocity, particleFactory)
{
    var ps = new ParticleSystem();
    ps.initialize(initialPos, initialVelocity, particleFactory);
    this.particleSystems.push(ps);
};

ParticleManager.prototype.update = function(timeStep)
{
    for (var i=0; i < this.particleSystems.length; ++i) {
        var ps = this.particleSystems[i];
        ps.update(timeStep);
        if (ps.isAlive === false) {
            this.particleSystems.splice(i, 1);
            i--;
        }
    }
};

ParticleManager.prototype.draw = function(gfxContext)
{
    for (var i=0; i < this.particleSystems.length; ++i) {
        var ps = this.particleSystems[i];
        ps.draw(gfxContext);
    }
};
