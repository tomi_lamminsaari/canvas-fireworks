
function Particle()
{
    this.pos = new Vec2();
    this.velocity = new Vec2();
    this.color = newRgbColor(255,0,0);
    this.lifeLeft = 5.0;
    this.orginalLife = 5.0;
    this.isAlive = true;
    this.particleSize = 2;
}

/**
 * 
 * @param {Vec2} pos
 * @param {float} lifeTime
 * @param {Vec2} velocity
 */
Particle.prototype.initialize = function(pos, lifeTime, velocity)
{
    this.lifeLeft = lifeTime;
    this.originalLife = lifeTime;
    this.isAlive = true;
    this.pos.setXY(pos.x, pos.y);
    this.velocity.setXY(velocity.x, velocity.y);
};


Particle.prototype.update = function(timeStep)
{
    if (this.isAlive) {
        this.pos.addScaled(this.velocity, timeStep);
        this.velocity.scale(1.0 - (0.2 * timeStep));
        this.velocity.addScaled(globalGravity, timeStep);
        
        this.lifeLeft -= timeStep;
        if (this.lifeLeft < 0.0) {
            this.isAlive = false;
        }
    }
    return this.isAlive;
};

/**
 * 
 * @param {type} gfxContext
 * @param {Vec2} translation
 */
Particle.prototype.draw = function(gfxContext, translation)
{
    var px = Math.floor(this.pos.x + translation.x);
    var py = Math.floor(this.pos.y + translation.y);
    var a = (this.lifeLeft / this.orginalLife) * 1.0;
    var styleStr = this.color.getFillStyleAlpha(a);
    gfxContext.fillStyle = this.color.getFillStyleAlpha(a);
    gfxContext.fillRect(px, py, this.particleSize, this.particleSize);
};

