
var primaryCanvas = $("#displayCanvas")[0];
var canvasContext = primaryCanvas.getContext("2d");
var appEngine;
var lastTimeStamp;
var gfxPool = new GfxPool();


var skyPos = new Vec2();
var skyMovementSpeed = new Vec2();

function getTimeStamp()
{
    return window.performance && window.performance.now ? window.performance.now() : new Date().getTime();
}

function update(timeStep)
{
    if (appEngine != null) {
        appEngine.update(timeStep);
    }
    skyPos.addScaled(skyMovementSpeed, timeStep);
    if (skyPos.y <= 0) {
        skyPos.setXY(0, 600);
    }
}

function render()
{
    canvasContext.drawImage(gfxPool.nightSkyImage, 
                            Math.floor(skyPos.x), Math.floor(skyPos.y), 
                            800, 600, 0, 0, 800, 600);
    appEngine.draw(canvasContext);
}

function frame()
{
    var now = getTimeStamp();
    var dt = (now - lastTimeStamp) / 1000.0;
    
    update(dt);
    render();
    lastTimeStamp = now;
    
    requestAnimationFrame(frame);
}


function initialize()
{
    skyPos = newVec2(0, 600);
    skyMovementSpeed = newVec2(800.0, -600.0);
    skyMovementSpeed.scale(0.01);
    
    appEngine = new Engine();
    var rocket = createRocket(appEngine, newVec2(400,600), newVec2(0.0, -95.0), 3.0);
    appEngine.addRocket(rocket);
}

function mainFunc()
{
    console.log("Application started!");
    lastTimeStamp = getTimeStamp();
    initialize();
    
    frame();
}


mainFunc(); // Start the app

