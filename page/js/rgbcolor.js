
function RgbColor()
{
    this.r = 0;
    this.g = 0;
    this.b = 0;
}

RgbColor.prototype.set = function(redVal, greenVal, blueVal)
{
    this.r = redVal;
    this.g = greenVal;
    this.b = blueVal;
};

RgbColor.prototype.clone = function()
{
    var tmp = new RgbColor();
    tmp.r = this.r;
    tmp.g = this.g;
    tmp.b = this.b;
    return tmp;
}

RgbColor.prototype.getFillStyle = function()
{
    return "rgb(" + this.r + "," + this.g + "," + this.b + ")";
};

RgbColor.prototype.getFillStyleAlpha = function(a)
{
    return "rgba(" + this.r + "," + this.g + "," + this.b + "," + a + ")";
};

function newRgbColor(redVal, greeVal, blueVal)
{
    var tmp = new RgbColor();
    tmp.set(redVal, greeVal, blueVal);
    return tmp;
}
