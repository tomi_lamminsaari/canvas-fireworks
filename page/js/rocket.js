
var ROCKET_TYPE_NIGHBLUE = 0;
var ROCKET_TYPE_COLORFUL = 1;
var ROCKET_TYPE_BIGWHITE = 2;
var ROCKET_TYPE_SUBROCKETS = 3;

function Rocket()
{
    this.pos = new Vec2();
    this.velocity = new Vec2();
    this.explodeCounter = 5.0;
    this.emitPropulsionCounter = 0.2;
    this.isAlive = true;
    this.engine = null;
    this.propulsionParticleFactory = new ParticleFactoryPropulsionFire();
    this.rocketType = ROCKET_TYPE_BIGWHITE;
}

Rocket.prototype.initialize = function(startPos, startDir, timeToLive)
{
    this.pos = startPos.clone();
    this.velocity = startDir.clone();
    this.explodeCounter = timeToLive;
    this.isAlive = true;
    
    var propulsionDir = startDir.clone();
    propulsionDir.rotateDegs(180.0);
    propulsionDir.scale(1.5);
    this.propulsionParticleFactory.setPropulsionDirection(propulsionDir);
};

Rocket.prototype.setRocketType = function(t)
{
    this.rocketType = t;
};

Rocket.prototype.update = function(timeStep)
{
    if (this.isAlive) {
        this.pos.addScaled(this.velocity, timeStep);
        this.explodeCounter -= timeStep;
        if (this.explodeCounter <= 0.0) {
            this.isAlive = false;
            
            this.explodeRocket();   
        }
        
        this.emitPropulsionCounter -= timeStep;
        if (this.emitPropulsionCounter <= 0.0) {
            var particleMan = this.engine.getParticleManager();
            particleMan.addSystem(this.pos, this.velocity, this.propulsionParticleFactory);
            this.emitPropulsionCounter = 0.03;
        }
    }
};

Rocket.prototype.draw = function(gfxContext)
{
    if (this.isAlive) {
        gfxContext.fillStyle = "#AAA";
        gfxContext.fillRect(Math.floor(this.pos.x)-1, Math.floor(this.pos.y)-1, 2, 2);
    }
};

Rocket.prototype.explodeRocket = function()
{
    if (this.rocketType == ROCKET_TYPE_SUBROCKETS) {
        var dirV = this.velocity.clone();
        dirV.scale(0.5);
        dirV.rotateDegs(45.0);
        dirV.add(this.velocity);
        this.engine.addRocket(createRocket(this.engine, this.pos, dirV, 0.7, ROCKET_TYPE_BIGWHITE));
        
        dirV = this.velocity.clone();
        dirV.rotateDegs(-45.0);
        dirV.add(this.velocity);
        this.engine.addRocket(createRocket(this.engine, this.pos, dirV, 0.8, ROCKET_TYPE_BIGWHITE));
        
        dirV = this.velocity.clone();
        dirV.rotateDegs(90.0);
        dirV.add(this.velocity);
        this.engine.addRocket(createRocket(this.engine, this.pos, dirV, 0.6, ROCKET_TYPE_BIGWHITE));
        
        dirV = this.velocity.clone();
        dirV.rotateDegs(-90.0);
        dirV.add(this.velocity);
        this.engine.addRocket(createRocket(this.engine, this.pos, dirV, 0.65, ROCKET_TYPE_BIGWHITE));
        
    } else {
        var particleMan = this.engine.getParticleManager();
        var particleFactory = getRocketParticleFactory(this.rocketType);
        particleMan.addSystem(this.pos, this.velocity, particleFactory);
        
        this.flashScreen();
        this.spawnLightBloom();
    }
}

Rocket.prototype.flashScreen = function()
{
    // Make the screen flash.
    var flashElem = $("#lightFlash");
    flashElem.removeClass("rocketExplodeAnim");
    flashElem[0].offsetWidth = flashElem[0].offsetWidth;
    flashElem.addClass("rocketExplodeAnim");
}

Rocket.prototype.spawnLightBloom = function()
{
    var bloomObject = new LightBloom();
    bloomObject.initialize(this.pos, this.velocity);
    this.engine.addDynamicObject(bloomObject);
}

function createRocket(engine, startPos, startDir, timeToLive, rocketType)
{
    var r = new Rocket();
    r.initialize(startPos, startDir, timeToLive);
    r.engine = engine;
    r.setRocketType(rocketType);
    return r;
}


function getRandomSingleRocketType()
{
    var v = Math.random();
    var treshold = 1.0 / 4.0;
    if (v < treshold) {
        return ROCKET_TYPE_NIGHBLUE;
    } else if (v < 2.0*treshold) {
        return ROCKET_TYPE_COLORFULL;
    } else if (v < 3.0*treshold) {
        return ROCKET_TYPE_BIGWHITE
    }
    return ROCKET_TYPE_SUBROCKETS;
}

function getRocketParticleFactory(rocketType)
{
    if (rocketType == ROCKET_TYPE_NIGHBLUE) {
        return new ParticleFactoryRocket(80, ROCKET_COLOR_SCHEME_BLUE);
    } else if (rocketType == ROCKET_TYPE_COLORFUL) {
        return new ParticleFactoryRocket(80, ROCKET_COLOR_SCHEME_RGB);
    } else {
        return new ParticleFactoryRocket(160, ROCKET_COLOR_SCHEME_WHITE);
    }
}