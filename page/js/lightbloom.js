
function LightBloom()
{
    this.pos = new Vec2();
    this.velocity = new Vec2();
    this.isAlive = true;
    this.lifeCounter = 0;
    this.initialLifeCounter = 0.5;
}

LightBloom.prototype.initialize = function(initialPos, initialVelocity)
{
    this.isAlive = true;
    this.pos = initialPos.clone();
    this.velocity = initialVelocity.clone();
    this.initialLifeCounter = 0.8;
    this.lifeCounter = this.initialLifeCounter;
};

LightBloom.prototype.update = function(timeStep)
{
    this.pos.addScaled(this.velocity, timeStep);
    this.velocity.addScaled(globalGravity, timeStep);
    this.lifeCounter -= timeStep;
    if (this.lifeCounter < 0.0) {
        this.isAlive = false;
    }
};

LightBloom.prototype.draw = function(gfxCanvas)
{
    if (this.isAlive) {
        var posX = this.pos.x - (gfxPool.lightBloomImage.width / 2);
        var posY = this.pos.y - (gfxPool.lightBloomImage.height / 2);
        var a = (this.lifeCounter / this.initialLifeCounter) * 0.3;
        gfxCanvas.globalAlpha = a;
        gfxCanvas.drawImage(gfxPool.lightBloomImage, Math.floor(posX), Math.floor(posY));
        gfxCanvas.globalAlpha = 1.0;
    }
};

