
function Engine()
{
    this.particleManager = new ParticleManager();
    this.objects = new Array();
    this.rocketTime = 2.0;
}

Engine.prototype.addRocket = function(rocketObj)
{
    this.objects.push(rocketObj);
};

Engine.prototype.addDynamicObject = function(obj)
{
    this.objects.push(obj);
};

Engine.prototype.update = function(timeStep)
{
    this.particleManager.update(timeStep);
    for (var i=0; i < this.objects.length; ++i) {
        var obj = this.objects[i];
        obj.update(timeStep);
        if (!obj.isAlive) {
            this.objects.splice(i, 1);
            --i;
        }
    }
    
    this.rocketTime -= timeStep;
    if (this.rocketTime <= 0.0) {
        this.rocketTime = 0.5 + 1.0 * Math.random();
        this.generateNewRocket();
    }
};

Engine.prototype.draw = function(gfxContext)
{
    for (var i=0; i < this.objects.length; ++i) {
        var obj = this.objects[i];
        obj.draw(gfxContext);
    }
    this.particleManager.draw(gfxContext);
};

Engine.prototype.getParticleManager = function()
{
    return this.particleManager;
};

Engine.prototype.generateNewRocket = function()
{
    var rocketDir = newVec2(0.0, -95.0);
    var rotAngle = (60.0 * Math.random()) - 30.0;
    rocketDir.rotateDegs(rotAngle);
    var lifeTime = 2.5 + (Math.random() * 3.0);
    var rocket = createRocket(appEngine, newVec2(400,600), rocketDir, lifeTime);
    var randv = Math.random();
    var t = ROCKET_TYPE_NIGHBLUE;
    if (randv < 0.3) {
        t = ROCKET_TYPE_COLORFUL;
    } else if (randv < 0.6) {
        t = ROCKET_TYPE_SUBROCKETS;
    }
    
    rocket.setRocketType(t);
    this.addRocket(rocket);
};
