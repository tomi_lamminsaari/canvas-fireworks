
function ParticleSystem()
{
    this.particles = new Array();
    this.isAlive = true;
    this.position = new Vec2();
    this.velocity = new Vec2();
    
}

ParticleSystem.prototype.initialize = function(pos, velo, particleFactory)
{
    this.isAlive = true;
    this.position = pos.clone();
    this.velocity = velo.clone();
    particleFactory.createParticles(this);
}

/**
 * 
 * @param {Particle} p
 */
ParticleSystem.prototype.addParticle = function(p)
{
    this.particles.push(p);
}

ParticleSystem.prototype.update = function(timeStep)
{
    if (this.isAlive === false) {
        return;
    }
    this.position.addScaled(this.velocity, timeStep);
    this.velocity.addScaled(globalGravity, timeStep);
    
    var deadCount = 0;
    for (var i=0; i < this.particles.length; ++i) {
        if (this.particles[i].update(timeStep) === false) {
            deadCount++;
        }
    }
    if (deadCount >= this.particles.length) {
        this.isAlive = false;
    }
};

ParticleSystem.prototype.draw = function(gfxContext)
{
    if (this.isAlive) {
        for (var i=0; i < this.particles.length; ++i) {
            this.particles[i].draw(gfxContext, this.position);
        }
    }
};
