
function ParticleFactoryPropulsionFire()
{
    this.propulsionDir = new Vec2();
    
    this.setPropulsionDirection = function(dirVec) {
        this.propulsionDir = dirVec.clone();
    }
    
    /**
     * 
     * @param {ParticleSystem} particleSystem
     * @returns {undefined}
     */
    this.createParticles = function(particleSystem) {
        var dirVec = new Vec2();
        var rotAngle = 0.0;
        for (var i=0; i < 8; ++i) {
            dirVec.setXY(this.propulsionDir.x, this.propulsionDir.y);
            rotAngle = (Math.random() * 20.0) - 10.0;
            dirVec.rotateDegs(rotAngle);
            
            p = new Particle();
            p.initialize(newVec2(0.0, 0.0), 0.57, dirVec);
            p.color = newRgbColor(255,255,80);
            p.particleSize = 1;
            
            particleSystem.addParticle(p);
        }
    }
}

